FROM alpine:edge
MAINTAINER T.K "oonumchi@gmail.com"

RUN apk --no-cache add mysql mysql-client tzdata

# タイムゾーンを日本時間に設定する
RUN cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
RUN apk del tzdata

RUN mkdir -p /run/mysqld
RUN chown -R mysql:mysql /run/mysqld
RUN chown -R mysql:mysql /var/lib/mysql
# MariaDBの初期化
RUN mysql_install_db --datadir=/var/lib/mysql --user=mysql
# my.cnfのコピー
COPY mysql/my.cnf /etc/mysql/my.cnf

ADD scripts/run.sh /scripts/run.sh
RUN chmod -R 755 /scripts

# ポートの公開
EXPOSE 3306

# コンテナ生成時に実行するコマンド
ENTRYPOINT ["/scripts/run.sh"]
